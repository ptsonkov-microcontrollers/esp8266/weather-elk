# ESP8266 send data to Elasticsearch

ELK current version - 7.9.3

https://github.com/patw/esp8266-dht22-kibana

Copy  
__examples/code_secrets-example.h__  
to  
__src/code_secrets.h__  
and set correct values

## Prepare Elasticsearch

x. Export variables
```
export ESHOST='<ElasticsearchIP>'
export ESPORT='<ElasticsearchPORT>'
export ESAUTH='-u <usenmane>:<password>'
```

x. Add ingest pipeline
```
PUT _ingest/pipeline/autots {"description":"add @timestamp","processors":[{"set":{"field":"@timestamp","value":"{{_ingest.timestamp}}"}}]}
```
```
# curl -X PUT $ESAUTH "http://$ESHOST:$ESPORT/_ingest/pipeline/autots" -H 'Content-Type: application/json' -d'{"description":"add @timestamp","processors":[{"set":{"field":"@timestamp","value":"{{_ingest.timestamp}}"}}]}'
```
$ curl -X GET api/kibana/dashboards/export?dashboard=942dcef0-b2cd-11e8-ad8e-85441f0c2e5c
$ curl -X GET "http://10.200.1.10:5601/api/kibana/dashboards/export?dashboard=e38c7a20-2204-11eb-b629-01cf9eaede6b"
