#include <Adafruit_BME280.h>
#include <Adafruit_I2CDevice.h>
#include <Adafruit_Sensor.h>
#include <ESP8266WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include "code_secrets.h"

// Declare of BME280 sensor
Adafruit_BME280 bme;

// Set station name and refresh interval
const String node_name = "outside";
const int refresh_seconds = 30;

// Set WiFi name and password
const char *ssid = SECRET_WIFI_SSID;
const char *password = SECRET_WIFI_PASS;

// Elasticsearch address and port
const char *host = SECRET_MQTT_SERVER;
const int httpsPort = 9200;

// // ES User/Pass in Base64 for basic auth
// // Usually in the format user:pass
// const char *userpass = "<changeme>";

// Create variable to store sensor data
float temperature, humidity, pressure;
// String to hold date for dynamic index
String timestamp;

// Define NTP Client
WiFiUDP ntpUDP;
const char *ntpServer = "pool.ntp.org";
NTPClient timeClient(ntpUDP, ntpServer);
// Set offset time in seconds to adjust for your timezone, for example:
// GMT +1 = 3600
// GMT +8 = 28800
// GMT -1 = -3600
// GMT 0 = 0s
int timeZome = +2;

// Forwarding function declaration
String sensor_json();
void get_sensor_data();
void index_time();

void setup() {

  Serial.begin(115200);

  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  bme.begin(0x76);

  // Initialize a NTPClient to get time
  timeClient.begin();
  // Calculate timezone shift in seconds
  int tz = (timeZome * 60) * 60;
  timeClient.setTimeOffset(tz);
}

void loop() {

  // Set index dynamic part
  index_time();

  // Use WiFiClient class to create insecure connection
  WiFiClient client;
  // // Use WiFiClientSecure class to create TLS connection
  // // if needed, include header  "#include <WiFiClientSecure.h>"
  // WiFiClientSecure client;
  Serial.print("connecting to ");
  Serial.println(host);
  if (!client.connect(host, httpsPort)) {
    Serial.println("connection failed");
    delay(3000);
    return;
  }

  String url =
      "/esp8266_thp_" + node_name + "_" + timestamp + "/sensor?pipeline=autots";
  Serial.print("requesting URL: ");
  Serial.println(url);

  String doc = sensor_json();

  Serial.println("Sending Document:");
  Serial.println(doc);
  Serial.println(doc.length());

  if (doc.length() > 0) {
    client.print(String("POST ") + url + " HTTP/1.1\r\n" + "Host: " + host +
                 "\r\n" + "User-Agent: esp8266temp\r\n" +
                 "WWW-Authenticate: Basic realm=\"security\"\r\n" +
                 // "Authorization: Basic " + userpass + "\r\n" +
                 "Connection: close\r\n" +
                 "Content-Type: application/json\r\n" +
                 "Content-Length: " + doc.length() + "\r\n\r\n" + doc + "\r\n");

    // Serial.println("request sent");
    while (client.connected()) {
      String line = client.readStringUntil('\n');
      if (line == "\r") {
        Serial.println("headers received");
        break;
      }
    }
    String line = client.readStringUntil('\n');
    Serial.println("reply was:");
    Serial.println(line);
    delay(refresh_seconds * 1000);
  } else {
    delay(1000);
  }
}

//==============================================================================
//=== Supporting functions

String sensor_json() {

  get_sensor_data();
  String doc = "";

  if (isnan(temperature) || isnan(humidity) || isnan(pressure)) {
    return doc;
  } else {
    doc = "{\"location\":\"" + node_name + "\"," +
          "\"temperature\":" + temperature + "," + "\"humidity\":" + humidity +
          "," + "\"pressure\":" + pressure + "}";
  }

  return doc;
}

void get_sensor_data() {
  temperature = bme.readTemperature();
  humidity = bme.readHumidity();
  pressure = bme.readPressure() / 100.0F;
}

void index_time() {
  timeClient.update();
  unsigned long epochTime = timeClient.getEpochTime();
  // Create time structure
  struct tm *ptm;
  ptm = gmtime((time_t *)&epochTime);

  String year, month, day;

  int y = ptm->tm_year + 1900;
  int m = ptm->tm_mon + 1;
  int d = ptm->tm_mday;
  Serial.print("date: ");

  year = String(y);

  if (m < 10) {
    month = "0" + String(m);
  } else {
    month = String(m);
  }

  if (d < 10) {
    day = "0" + String(d);
  } else {
    day = String(d);
  }

  timestamp = year + month + day;
}
